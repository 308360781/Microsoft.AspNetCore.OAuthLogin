﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Microsoft.AspNetCore.OAuthLogin.Config
{
    /// <summary>
    /// Configuration 必须赋值
    /// </summary>
    public class OAuth2Options
    {
        /// <summary>
        /// 是否启用微信登陆
        /// 默认启用
        /// </summary>
        public bool Wechat { get; set; }

        /// <summary>
        /// 是否启用QQ登陆
        /// 默认启用
        /// </summary>
        public bool QQ { get; set; }

        /// <summary>
        /// 是否启用Weibo登陆
        /// 默认启用
        /// </summary>
        public bool Weibo { get; set; }

        /// <summary>
        /// 加载配置
        /// </summary>
        public IConfiguration Configuration { get; set; }
    }

    /// <summary>
    /// 注册服务扩展
    /// </summary>
    public static class IServiceCollectionExtension
    {
        /// <summary>
        /// 启用第三方登陆服务注册,默认都启用
        /// </summary>
        /// <param name="services"></param>
        /// <param name="options">配置</param>
        /// <returns></returns>
        public static IServiceCollection AddOAuth2(this IServiceCollection services, Action<OAuth2Options> options)
        {
            var obj = new OAuth2Options() { Wechat = true, QQ = true, Weibo = true };
            options(obj);
            if (obj.Wechat)
                services.AddSingleton(typeof(Wechat));
            if (obj.Weibo)
                services.AddSingleton(typeof(Weibo));
            if (obj.QQ)
                services.AddSingleton(typeof(QQ));

            //添加配置文件
            services.Configure<OAuthCredentials>(obj.Configuration.GetSection("OAuthCredentials"));

            return services;
        }


    }
}
