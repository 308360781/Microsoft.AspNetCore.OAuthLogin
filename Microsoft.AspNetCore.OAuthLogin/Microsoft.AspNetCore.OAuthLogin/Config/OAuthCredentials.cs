﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Microsoft.AspNetCore.OAuthLogin.Config
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class OAuthCredentials
    {
        /// <summary>
        /// Wechat
        /// </summary>
        public CredentialSetting Wechat { get; set; }

        /// <summary>
        /// QQ
        /// </summary>
        public CredentialSetting QQ { get; set; }

        /// <summary>
        /// Weibo
        /// </summary>
        public CredentialSetting Weibo { get; set; }

    }

    /// <summary>
    /// 
    /// </summary>
    public class CredentialSetting
    {
        /// <summary>
        /// AppKey
        /// </summary>
        public string client_id { get; set; }

        /// <summary>
        /// AppSecret
        /// </summary>
        public string client_secret { get; set; }

    }

}
