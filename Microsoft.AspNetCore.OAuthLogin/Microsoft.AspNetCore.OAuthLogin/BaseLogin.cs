﻿using Microsoft.AspNetCore.Http;
using System;

namespace Microsoft.AspNetCore.OAuthLogin
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class BaseLogin
    { 
        private IHttpContextAccessor _httpContextAccessor;

        public BaseLogin(IHttpContextAccessor httpContextAccessor)
        {
            this._httpContextAccessor = httpContextAccessor;
        }

        private const string CODE = "code";

        /// <summary>
        /// 回调获取的code值
        /// </summary>
        protected string AuthorizeCode
        {
            get
            {
                var result = _httpContextAccessor.HttpContext.Request.Query[CODE].ToString();

                if (!string.IsNullOrEmpty(result)) return result;

                return string.Empty;
            }
        }

        /// <summary>
        /// 授权码回调地址
        /// 当前请求的url地址
        /// </summary>
        protected string CallbackUrl
        {
            get
            {
                return $"{_httpContextAccessor.HttpContext.Request.Scheme}://{_httpContextAccessor.HttpContext.Request.Host.Value}{_httpContextAccessor.HttpContext.Request.Path.Value}" ;
            }
        }

        /// <summary>
        /// 跳转的地址
        /// </summary>
        public abstract string Authorize_Url { get; }

    }
}
