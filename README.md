
# Microsoft.AspNetCore.OAuthLogin

#### 项目介绍
适用 Asp.net core 的 Weixin，Weibo，QQ登陆

#### 安装教程

1. PM> Install-Package CTS.Microsoft.AspNetCore.OAuthLogin
2. 克隆项目将代码拷贝至开发的项目当中

#### 使用说明

1. 配置appsettings.json文件

`{
  "Logging": {
    "IncludeScopes": false,
    "LogLevel": {
      "Default": "Debug",
      "System": "Information",
      "Microsoft": "Information"
    }
  },
  "OAuthCredentials": {
    "QQ": {
      "client_id": "",
      "client_secret": ""
    },
    "Wechat": {
      "client_id": "",
      "client_secret": ""
    },
    "Weibo": {
      "client_id": "",
      "client_secret": ""
    },
    "FaceBook": {
      "client_id": "",
      "client_secret": ""
    },
    "KaKao": {
      "client_id": "",
      "client_secret": ""
    }
  }
}`
2. 在Startup.cs配置微博、微信、QQ的client_id、client_secret
    `services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<Wechat>();
            services.AddSingleton<QQ>();
            services.AddSingleton<Weibo>();
            services.Configure<OAuthCredentials>(Configuration.GetSection("OAuthCredentials"));`
    


3. xxxx
`
public class OAuthController : Controller
    { 

        private Wechat _wechat; 
        private QQ _qq;
        private Weibo _weibo;

        public OAuthController(Wechat wechat,
            QQ  qq,
            Weibo weibo)
        {
            this._qq = qq;
            this._weibo = weibo;
            this._wechat = wechat;
        }

        public IActionResult QQ()
        {
            var res = _qq.Authorize();

            if (res != null && res.code == 0)
            {
                return RedirectToLogin(new
                {
                    channel = "qq",
                    code = 0,
                    user = new
                    {
                        uid = res.result.Value<string>("openid"),
                        name = res.result.Value<string>("nickname"),
                        img = res.result.Value<string>("figureurl"),
                        token = res.token
                    }
                });
            }

              return Redirect(_wechat.Authorize_Url);
        }

        public IActionResult Wechat()
        {
            var res = new Wechat(_contextAccessor).Authorize();

            if (res != null && res.code == 0)
            {
                return RedirectToLogin(new
                {
                    channel = "wechat",
                    code = 0,
                    user = new
                    {
                        uid = res.result.Value<string>("uid"),
                        name = res.result.Value<string>("nickname"),
                        img = res.result.Value<string>("headimgurl"),
                        token = res.token
                    }
                });
            }

            return View();
        }

        public IActionResult Weibo()
        {
            var res = new Weibo(_contextAccessor).Authorize();

            if (res != null && res.code == 0)
            {
                return RedirectToLogin(new
                {
                    channel = "weibo",
                    code = 0,
                    user = new
                    {
                        uid = res.result.Value<string>("idstr"),
                        name = res.result.Value<string>("name"),
                        img = res.result.Value<string>("profile_image_url"),
                        token = res.token
                    }
                });
            }

             return Redirect(_qq.Authorize_Url);
        }

        public IActionResult Facebook()
        {
            var res = new Facebook(_contextAccessor).Authorize();

            if (res != null && res.code == 0)
            {
                return RedirectToLogin(new
                {
                    channel = "facebook",
                    code = 0,
                    user = new
                    {
                        uid = res.result.Value<string>("id"),
                        name = res.result.Value<string>("name"),
                        img = res.result["picture"]["data"].Value<string>("url"),
                        token = res.token
                    }
                });
            }

            return Redirect(_weibo.Authorize_Url);
        }

       
        RedirectResult RedirectToLogin(object _entity)
        {  

            return RedirectToRoute("centerIndex");
        }
    }
`