﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Microsoft.AspNetCore.OAuthLogin.Model
{
    /// <summary>
    /// 验证结果
    /// </summary>
    [Serializable]
    public class OAuthResult
    {
        /// <summary>
        /// 0：成功
        /// </summary>
        public int code { get; set; }

        /// <summary>
        /// 错误描述
        /// </summary>
        public string errormsg { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public JObject Token { get; set; }

        /// <summary>
        /// 返回的用户信息
        /// </summary>
        public JObject User { get; set; }
    }
}
